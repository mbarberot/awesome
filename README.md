# Front

- [Frontend Handbook 2019](https://frontendmasters.com/books/front-end-handbook/2019/)
- [HTML/CSS Styleguide](https://codeguide.co/)

## CSS/SASS

- [Mana](https://github.com/andrewgioia/Mana): Font with Magic The Gathering mana symbols
- [NES.css](https://github.com/nostalgic-css/NES.css): NES-style CSS Framework
- [Milligram](https://milligram.io/): Minimalist CSS Framework
- [loaders.css](https://connoratherton.com/loaders): Collection of CSS loaders
- [How to center in CSS](http://howtocenterincss.com/)
- [CSS reference](https://tympanus.net/codrops/css_reference/)
- [The 7:1 Pattern in SASS](https://sass-guidelin.es/#the-7-1-pattern)

## Javascript

- [Airbnb Styleguide](https://github.com/airbnb/javascript)
- [ES6 Cheatsheet](https://github.com/DrkSephy/es6-cheatsheet)
- [Mailcheck](https://github.com/mailcheck/mailcheck): Lib that checks emails and suggests a correction (ie: "foo@hotnail.com" => "did you mean foo@hotmail.com")
- [miragejs](https://miragejs.com): API mock library
- [miragejs : graphql](https://github.com/miragejs/graphql)

## Ember

- [Ember Observer](https://emberobserver.com): Addon search engine
- [Ember CLI](https://ember-cli.com/user-guide/)
- [Ember JS](https://api.emberjs.com/ember/release)
- [Building a progressive web app with ember](https://madhatted.com/2017/6/16/building-a-progressive-web-app-with-ember)
- [Ember Pods](https://www.programwitherik.com/ember-pods/)
- [Ember Typescript](https://ember-cli-typescript.com/docs)

## Vue

- [Blog of Markus Oberlehner](https://markus.oberlehner.net/blog/): A good blog talking about tests and best practices with Vue.js

## Others

- [Eleventy](https://www.11ty.dev/): a simple static website generator
- [Pa11y](https://github.com/pa11y/pa11y): an accessibility checker to use with command line or in a CI pipeline

# Backend

## Java

- [TechGeekNext](https://www.techgeeknext.com/): The Java part of this website has a good summary of the new features of every JDK since 11
- [Rest Assured](https://github.com/rest-assured/rest-assured/wiki/Usage): A lib for REST API testing
- [JBang](https://www.jbang.dev/): A tool to create and package self-executable scripts in Java

## Kotlin

- [Official Documentation](https://kotlinlang.org/docs/reference/)

# System

## Linux

- [Bash guide](https://github.com/Idnan/bash-guide)

## Rapsberry Pi

- [Increase SD Card lifetime (fr)](https://korben.info/raspberry-pi-allonger-la-duree-de-vie-de-vos-cartes-sd.html)
- [Pi Hole](https://pi-hole.net/)
- [Raspberry Pi headless setup](https://linuxhandbook.com/raspberry-pi-headless-setup/)
- [Docker on Raspberry Pi](https://blog.alexellis.io/getting-started-with-docker-on-raspberry-pi/)

# Tools

## Git

- [Git tips](https://github.com/git-tips/tips)
- [How to undo (almost) anything with Git](https://github.blog/2015-06-08-how-to-undo-almost-anything-with-git/)

## CI

## Vim

- [Vim Cheatsheet](https://vim.rtorr.com/)
- [Vim Text Objects: The Definitive Guide](https://blog.carbonfive.com/2011/10/17/vim-text-objects-the-definitive-guide/)

# Misc

- [Regex 101](https://regex101.com/): A tool for testing/understanding regular expressions
- [Nginx snippets](https://github.com/lebinh/nginx-conf)
- [Ascii Title Generator](http://www.patorjk.com/software/taag/)

## Office

- [Overleaf](https://www.overleaf.com): SaaS for LateX
- [reveal.js](https://github.com/hakimel/reveal.js): A nice presentation tool using Javascript and Markdown
- [impress.js](https://impress.js.org): A presentation tool focused on animations
- [Excalidraw](https://excalidraw.com/): Collaborative, opensource, self-hostable whiteboard

## Data

- [MTGJson](https://mtgjson.com/): Magic The Gathering cards and sets
- [Enneagon (fr)](http://enneagon.org/phrases): A random text generator producing correct but meaningless sentences in french
- [Lorraine Ipsum (fr)](http://www.lorraine-ipsum.fr/): Play on words firstname & lastname generator
- [Dummy text](http://dummytext.com/): A JS lib to easily insert dummy text
- [Placemat](https://placem.at/): Image placeholders
- [Free videos (fr)](https://korben.info/videos-libres-de-droit.html)
- [List of public domain sources](https://github.com/neutraltone/awesome-stock-resources#public-domain)
- [Thestocks](http://thestocks.im): Royalties free resources
- [Fantasy Name Generators](https://www.fantasynamegenerators.com/)
- [CocoMaterial](https://cocomaterial.com/): Open source hand drown illustrations

## Hosting

- [MLab](https://mlab.com/welcome/): Free MongoDB hosting
- [Heroku](https://dashboard.heroku.com/apps): Free app hosting

# Games

- [NippleJS](https://yoannmoi.net/nipplejs/): A js lib to create joysticks UI
- [Gamepad API](https://developer.mozilla.org/en-US/docs/Web/API/Gamepad_API/Using_the_Gamepad_API)
- [Keith Burgun (blog)](keithburgun.net)

# Skills

- [Keyzen](http://wwwtyro.github.io/keyzen/): Typing game
- [ztype](https://zty.pe/): Typing game
- [monkeytype](https://monkeytype.com/): Typing game
- [Refactoring Guru](https://refactoring.guru/): Design pattern list
- [Kata-log](https://kata-log.rocks/): Katas by topic and constraints ideas
- [What is not to like about this code](https://www.jbrains.ca/sessions/whats-not-to-like-about-this-code): A mob progamming exercise to share practices and ease code reviews

## TDD / Tests

- [TDD: Au delà de l'intro (fr)](https://www.youtube.com/watch?v=RlkgetzDenI): An awesome talk about TDD from Alpes Craft by Romeu Moura
- [Trop de mock tue le test : ce que l'archi hexagonale a changé (fr)](https://www.youtube.com/watch?v=rSO1y3lCBfk): Another awesome talk about writing tests and its preconceived ideas.
- [Test Driven Development: By Example](https://www.pearson.com/us/higher-education/program/Beck-Test-Driven-Development-By-Example/PGM206172.html): writtent by Kent Beck, TDD's creator.

# Self promotion

- [Gitlab](https://gitlab.com/mbarberot)
- [Github](https://github.com/mbarberot)
- [Blog](https://mbarberot.gitlab.io/)

# Jobs

- [Annuaire Grand Est (fr)](https://docs.google.com/spreadsheets/d/1gaGA7iCn3_rCBKVHKfHAr65UWvPodm-1Mk-rbirg0ig/edit?invite=CNGClqgI#gid=2032141739)